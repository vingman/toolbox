#! /usr/bin/env python3
"""Moves lines in an xyz file"""
import re


class XYZ:
    """Holds coordinates from xyz file, along with any header or footer info"""
    def __init__(self, file_name):
        """Reads in XYZ file and stores the coordinates,
        along with any header data
        """
        self.head = []
        self.xyz = []
        self.foot = []
        tmp = file_name.split('.')
        self.original_name = "".join(tmp[:-1])
        self.file_extension = tmp[-1]

        header = True
        with open(file_name) as f:
            for line in f:
                if re.search('\S+(\s+-?\d+\.\d+){3}', line):
                    self.xyz += [line.rstrip()]
                    if header:
                        header = False
                elif header:
                    self.head += [line.rstrip()]
                else:
                    self.foot += [line.rstrip()]
        f.close()

    def insert_line(self, from_index, to_index):
        """Inserts line from_index before to_index"""
        # remove from_index atom(s) from list and store as temporary variable
        try:
            isinstance(from_index, int)
            tmp = self.xyz.pop(from_index)
        except TypeError:
            tmp = self.xyz[from_index[0]:from_index[1]+1]
            self.xyz = (self.xyz[:from_index[0]]
                        + self.xyz[from_index[1]+1:])
        # put removed atom(s) before to_index, or simply return (delete)
        try:
            isinstance(to_index, int)
            try:
                isinstance(tmp, list)
                self.xyz = self.xyz[:to_index] + tmp + self.xyz[to_index:]
            except TypeError:
                self.xyz = self.xyz[:to_index] + [tmp] + self.xyz[to_index:]
                tmp = [tmp]
        except TypeError:
            print(len(tmp), "lines deleted")

    def write(self, make_com=False, make_xyz=False):
        """Writes edited version to fname_edited.ext"""
        fname = self.original_name + "_edited"
        if make_com:
            head = ["#am1", "", "comment", "", "0 1"]
            foot = self.foot
            fname = fname + ".com"
        elif make_xyz:
            head = [str(len(self.xyz)), self.original_name]
            foot = []
            fname = fname + ".xyz"
        else:
            head = self.head
            foot = self.foot
            fname = fname + '.' + self.file_extension
        with open(fname, 'w') as f:
            for i in head:
                f.write(i + "\n")
            for i in self.xyz:
                f.write(i + "\n")
            for i in foot:
                f.write(i + "\n")


def main():
    """For commandline calling
    Usage: move-atoms.py movement_string file_names"""
    import argparse
    help_m = ("2/1,3-5/2 inserts atom 2 before atom 1 and then atoms 3-5 "
              + "before atom 2\n3-10/d deletes atoms 3-10")
    error_m = "error: badly formed movement_string"

    parser = argparse.ArgumentParser(description="Move lines in xyz file")
    parser.add_argument("--com", action='store_true',
                        help="output dummy com file")
    parser.add_argument("--xyz", action='store_true',
                        help="header changed to basic xyz, footer removed")
    parser.add_argument("movement_string", help=help_m)
    parser.add_argument("file_names", nargs='+')
    args = parser.parse_args()

    movements = []
    for s in args.movement_string.split(','):
        i = s.split('/')
        if len(i) != 2:
            exit(error_m)
        # from_index should be a number or a range
        try:
            i[0] = int(i[0]) - 1
        except ValueError:
            fi = i[0].split('-')
            if len(fi) != 2:
                exit(error_m)
            try:
                fi[0] = int(fi[0]) - 1
                fi[1] = int(fi[1]) - 1
            except ValueError:
                exit(error_m)
            i[0] = fi

        # to_index should be a number or d
        try:
            i[1] = int(i[1]) - 1
        except ValueError:
            if i[1] != 'd':
                exit(error_m)
        except IndexError:
            exit(error_m)
        movements += [i]

    xyz_list = []
    for fname in args.file_names:
        xyz_list += [XYZ(fname)]

    for x in xyz_list:
        for m in movements:
            x.insert_line(m[0], m[1])
        if args.com or args.xyz:
            if args.com:
                x.write(make_com=True)
            if args.xyz:
                x.write(make_xyz=True)
        else:
            x.write()


if __name__ == "__main__":
    main()
