#! /usr/bin/env python3
'''param_gen.py
Generates possibilites of x to model y from a given parameter set

Author: Victoria Ingman
'''

import csv
import itertools as it
import numpy as np

def get_params(params, n_params, multiply=True):
    '''
    actually performs the combinations and calculates the weight
    '''
    if multiply:
        # use combinations_with_replacement when multiplying parameters
        # since c*x != c*x^2
        new_params = it.combinations_with_replacement(params, n_params)
    else:
        # use combinations (without replacement) when adding parameters
        # since c*x == c*x + c*x
        new_params = it.combinations(params, n_params)
    rv = []
    for j in new_params:
        # calculate weights
        w = 0
        try:
            t = sum(j, [])
        except TypeError:
            t = j
        for k in params:
            if k in t:
                w += 1
        rv += [[list([j]), w]]
    return rv

def iterate(params, n_params):
    '''
    yields: [[param_ids]], int(weight)
    yields combinations of n_params by choosing <= max_params
    format of return value: ([[list-of-lists]], int)
    [[list-of-lists]]: the lists within the list-of-lists represent what should be added together, the elements within each list are indexes of parameters that should be multiplied together
    ie: [[x1,x2],[x3]] -> x1*x2 + x3
    int: the weight of the list-of-lists, where the weight is equal to the number of individual parameters present, although a single parameter^2 still only counts as one parameter
    '''
    # set up by first getting possible param lists without considering weight
    # this the multiplication part: {x}^2, {x}*{y} for now, only alows two
    # parameters multiplied together
    for i in range(1, n_params+1):
        for j in get_params(params, i):
            yield j


def get_results(x, params, max_params=-1, min_params=1):
    '''
    yields: trial_x_array, params_ids
    generates possibilites of x to model y from a given parameter set
    max_params defaults to N-columns, but asks for confirmation if over 6
    '''
    # TODO: include a minimum parameter attribute as well so one can slowly
    # increase the number of parameters without trying them all over again
    # Should also look into data persistence modules to help with this
    # \TODO

    # default limits number of parameters to the number of data columns
    # for large data sets, this should be explicitly set by the user!
    if max_params < 0:
        max_params = len(x[0])
    if max_params > len(x[0]):
        print("Parameters requested exceeds column dimensions.")
        exit()

    x = np.array(x, np.float)
    rv_data = []
    rv_labels = []
    # iterate over possible parameter combinations
    # iterate returns the ~column names~ in a special list format
    # this code uses the generated list to generate the new dataset
    idx = iterate(range(len(x[0])), max_params)
    for i, w in idx:
        # build trial_x array from trial parameter specifications
        try_x = np.array([])
        try_params = [] # for building new column names
        for j in i:
            term = np.array([]) # term building from parameters (for x1*x2)
            pm = "" # column name term
            for k in j:
                if term.size == 0:
                    term = x[:,k].copy()
                else:
                    term *= x[:,k]
                if len(j) == 1:
                    pm += "{}*".format(params[k])
                else:
                    pm += "({})*".format(params[k])
            # add column names
            pm = pm[:-1] # remove unnecessary ending "*"
            try_params.append(pm)
            # add terms to trial x-array
            if try_x.size == 0:
                try_x = term.copy()
            else:
                try_x = np.column_stack((try_x, term))
        rv_data += [try_x]
        rv_labels += try_params
    return rv_data, rv_labels


def gen_new_data(fname, y_col=False, id_col=0, has_headers=True, max_params=2):
    data = []
    params = []
    col_names = []
    ids = []
    with open(fname, newline='') as f:
        reader = csv.reader(f)
        row_count = -1
        for row in reader:
            if len(row) < 2:
                continue
            if ''.join(row).strip().startswith("#"):
                continue
            row_count += 1
            if row_count == 0 and has_headers:
                col_names = row
            elif row_count == 0:
                col_names = list(range(len(data[0])))
            else:
                data += [row[:id_col] + row[id_col+1:]]
                ids += [row[id_col]]
        params = col_names[:id_col] + col_names[id_col+1:]

    if y_col:
        x = data[:y_col] + data[y_col+1:]
    else:
        x = data
    new_data, new_labels = get_results(x, params, max_params)
    with open('param_gen.csv', 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(new_labels)
        for i in np.column_stack(new_data):
            writer.writerow(i)
    return


if __name__ == "__main__":
    gen_new_data("relative_filepath/data.csv", max_params=2)
